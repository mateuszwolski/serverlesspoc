package com.serverless;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.event.S3EventNotification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GetS3Handler implements RequestHandler<S3EventNotification, String> {
    private final Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public String handleRequest(S3EventNotification s3Event, Context context) {
        logger.info("Handle s3 event, {}", s3Event.toJson());
        return "Handled s3";
    }
}
