package com.serverless;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DynamoDBStreamTriggered implements RequestHandler<Object, String> {
    private final Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public String handleRequest(Object input, Context context) {
        logger.info("DynamoDB event, {}", input);
        return null;
    }
}
