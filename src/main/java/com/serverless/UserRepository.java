package com.serverless;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class UserRepository {
    private static final DynamoDBMapper mapper = DynamoDBManager.mapper();
    private Logger logger = LogManager.getLogger(this.getClass());

    private static volatile UserRepository instance;

    private UserRepository() {
    }

    public static UserRepository instance() {

        if (instance == null) {
            synchronized (UserRepository.class) {
                if (instance == null)
                    instance = new UserRepository();
            }
        }
        return instance;
    }

    public List<User> findAllUsers() {
        return mapper.scan(User.class, new DynamoDBScanExpression());
    }

//    public List<User> findUsersByBrand(String ) {
//
//        Map<String, AttributeValue> eav = new HashMap<>();
//        eav.put(":v1", new AttributeValue().withS(brand));
//
//        DynamoDBQueryExpression<User> query = new DynamoDBQueryExpression<User>()
//                .withIndexName(User.BOARD_INDEX)
//                .withConsistentRead(false)
//                .withKeyConditionExpression("board = :v1")
//                .withExpressionAttributeValues(eav);
//
//        return mapper.query(User.class, query);
//    }

    public void saveOrUpdateUser(User user) {

        mapper.save(user);
    }

    public User get(String id) {
        User user = null;

        HashMap<String, AttributeValue> av = new HashMap<String, AttributeValue>();
        av.put(":v1", new AttributeValue().withS(id));

        DynamoDBQueryExpression<User> queryExp = new DynamoDBQueryExpression<User>()
                .withKeyConditionExpression("id = :v1")
                .withExpressionAttributeValues(av);

        PaginatedQueryList<User> result = this.mapper.query(User.class, queryExp);
        if (result.size() > 0) {
            user = result.get(0);
            logger.info("User - get(): user - " + user.toString());
        } else {
            logger.info("User - get(): user - Not Found.");
        }
        return user;
    }

    public Boolean delete(String id) throws IOException {
        User user = null;

        // get product if exists
        user = get(id);
        if (user != null) {
            logger.info("User - delete(): " + user.toString());
            this.mapper.delete(user);
        } else {
            logger.info("User - delete(): user - does not exist.");
            return false;
        }
        return true;
    }

}
