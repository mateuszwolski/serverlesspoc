package com.serverless;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetUsersHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

    private final Logger logger = LogManager.getLogger(this.getClass());
    private final UserRepository userRepository = UserRepository.instance();

    @Override
    public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
        try {
            List<User> users = userRepository.findAllUsers();

            // send the response back
            return ApiGatewayResponse.builder()
                    .setStatusCode(200)
                    .setObjectBody(users)
                    .setHeaders(Collections.singletonMap("X-Powered-By", "List of users"))
                    .build();
        } catch (Exception ex) {
            logger.error("Error in listing users: " + ex);

            // send the error response back
            Response responseBody = new Response("Error in listing users: ", input);
            return ApiGatewayResponse.builder()
                    .setStatusCode(500)
                    .setObjectBody(responseBody)
                    .setHeaders(Collections.singletonMap("X-Powered-By", "List of users"))
                    .build();
        }
    }

    public ApiGatewayResponse handleGetRequest(Map<String, Object> input, Context context) {
        // send the response back
        logger.info("handleGetRequest");
        try {
            String userId = ((HashMap<String, String>) input.get("pathParameters")).get("id");
            logger.info("userID: " + userId);
            User users = userRepository.get(userId);

            return ApiGatewayResponse.builder()
                    .setStatusCode(200)
                    .setObjectBody(users)
                    .setHeaders(Collections.singletonMap("X-Powered-By", "List of users"))
                    .build();
        } catch (RuntimeException e) {
            logger.error("Wrong request " + e);

            // send the error response back
            Response responseBody = new Response("Wrong request pathParams: ", input);
            return ApiGatewayResponse.builder()
                    .setStatusCode(401)
                    .setObjectBody(responseBody)
                    .setHeaders(Collections.singletonMap("X-Powered-By", "List of users"))
                    .build();
        }
    }

    public ApiGatewayResponse handleCreateRequest(Map<String, Object> input, Context context) {
        // send the response back
        logger.info("handleCreateRequest: body " + input.get("body"));
        ObjectMapper om = new ObjectMapper();
        try {
            String name = om.readValue(input.get("body").toString(), NewUserRequest.class).getName();
            User user = new User();
            user.setName(name);
            userRepository.saveOrUpdateUser(user);
            return ApiGatewayResponse.builder()
                    .setStatusCode(200)
                    .setObjectBody(user)
                    .setHeaders(Collections.singletonMap("X-Powered-By", "List of users"))
                    .build();
        } catch (Exception e) {
            logger.error("Wrong request " + e);

            // send the error response back
            Response responseBody = new Response("Wrong request pathParams: ", input);
            return ApiGatewayResponse.builder()
                    .setStatusCode(401)
                    .setObjectBody(responseBody)
                    .setHeaders(Collections.singletonMap("X-Powered-By", "List of users"))
                    .build();
        }
    }

    private static class NewUserRequest {
        String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}